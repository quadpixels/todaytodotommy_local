package mobi.nool.todayTodo.data;

public final class People {
	private String name;
	private long SocialnetworkID;
	
	
	
	public People(String name, long socialnetworkID) {
		super();
		this.name = name;
		SocialnetworkID = socialnetworkID;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getSocialnetworkID() {
		return SocialnetworkID;
	}
	public void setSocialnetworkID(long socialnetworkID) {
		SocialnetworkID = socialnetworkID;
	}
}
