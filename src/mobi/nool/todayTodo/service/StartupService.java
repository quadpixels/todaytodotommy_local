package mobi.nool.todayTodo.service;

import android.content.Context;

/**
 * Service which handles jobs that need to be run when todo list starts up.
 *
 * @author david
 *
 */


public class StartupService {

	public synchronized void onStartupApplication(final Context context) {
		
	}
}
