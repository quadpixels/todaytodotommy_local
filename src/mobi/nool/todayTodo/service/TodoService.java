package mobi.nool.todayTodo.service;



import java.util.ArrayList;
import java.util.Date;

import mobi.nool.todayTodo.data.Todo;






/**
 * Service layer for {@link Task}-centered activities.
 * @author david
 * 
 */
public class TodoService {

	
	
	/**
	 * @param title Cannot be null
	 * @param description Could be null
	 * @param time using java.util.Date type to represent the time condition of the todo; Could be null. 
	 * @return the new todo entry
	 */
	public Todo addTodo(String title, String description,Date time){
		return null ;
	} 
	
	/**
	 * @param input User input in the textfield
	 * @return a list of existing title which matched the <parameter> input </parameter> 
	 */
	public ArrayList<String> getHistory(String input){
		return null;
		
	}
	
	/**
	 * @param id
	 * @return the matching todo entry
	 */
	public Todo fetchTodoByID(long id){
		return null;
		
	}
	
	/**
	 * Typically called on start up.
	 * @param start
	 * @param end
	 * @return A list of Todo whose deadline is between start and end
	 */
	public ArrayList<Todo> fetchTodoByTime(Date start, Date end){
		return null;
	}
	
	/**
	 * @param id
	 * @return false if Todo is already marked as finished
	 */
	public boolean finishTodo(long id){
		return false;
	}
	
	/**
	 * @param id
	 * @return false if Todo is already unfinished
	 */
	public boolean unfinishTodo(long id){
		return false;
		
	}
	
	/**
	 * @param t represents the modified todo object
	 * @return false if no <b>t</b>'s ID hasn't been found in Database
	 */
	public boolean updateTodo(Todo t){
		return false;
		
	}
	
	/**
	 * @param t
	 * @return false if no <b>t</b>'s ID hasn't been found in Database
	 */
	public boolean removeTodo(Todo t){
		return false;
		
	}
	
	/*public Todo fetchById(long id, Property<?>... properties) {
	        //return todoDao.fetch(id, properties);
	 }
	 
	 public boolean save(Todo item) {
	        //return TodoDao.save(item);
	 }
	 
	 public void delete(Todo item) {
	        if(!item.isSaved())
	            return;
	        else  {
	        	TodoDao.delete(item.getId());
	            item.setId(Task.NO_ID);
	 }
	 
	 public int update(Criterion where, Todo template) {
	        return TodoDao.update(where, template);
	 }*/
	
}
