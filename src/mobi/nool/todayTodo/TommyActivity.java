package mobi.nool.todayTodo;

import com.ljp.pathmenu.MyAnimations;
import com.ljp.pathmenu.MyAnimations.ANIM_CORNER;

import mobi.nool.todayTodo.ui.DraggableRect;
import mobi.nool.todayTodo.ui.DraggableRect.MyHandlerInterface;
import mobi.nool.todayTodoTommy.R;
import android.app.Activity;
import android.content.Context;
import android.opengl.Visibility;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class TommyActivity extends Activity {
	
	/* Stuff for PATH-like menu */
	private boolean isButtonsShown;
	private RelativeLayout composerButtonsWrapper;
	private ImageView composerButtonsShowHideButtonIcon;
	private RelativeLayout composerButtonsShowHideButton;
	
	Context mCtx;
	LinearLayout mLinearLayout_Inbox;
	LinearLayout mLinearLayout_Tasks;
	LinearLayout mLinearLayout_Inbox_Inner;
	LinearLayout mLinearLayout_Tasks_Inner;
	
	DraggableRect mDraggableDot_Inbox;
	DraggableRect mDraggableDot_Tasks;
	
	
	Button mButton1;
	EditText mEditText1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    Log.v("TommyView", "onCreate");
	    setContentView(R.layout.tommylayout);
	    mCtx = getApplicationContext();
	    mLinearLayout_Inbox_Inner = (LinearLayout) findViewById(R.id.tommyLinearLayout_inbox_inner);
	    mLinearLayout_Tasks_Inner = (LinearLayout) findViewById(R.id.tommyLinearLayout_tasks_inner);
	    
	    mLinearLayout_Inbox = (LinearLayout) findViewById(R.id.tommyLinearLayout_inbox);
	    mLinearLayout_Tasks = (LinearLayout) findViewById(R.id.tommyLinearLayout_tasks);
	    
	    mButton1 = (Button) findViewById(R.id.tommyButton1);
	    mButton1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				addOneDummyData();
			}
		});
	    mEditText1 = (EditText) findViewById(R.id.tommyEditText1);
	    
	    mDraggableDot_Inbox = (DraggableRect) findViewById(R.id.drag_dot_inbox);
	    mDraggableDot_Tasks = (DraggableRect) findViewById(R.id.drag_dot_tasks);
	    
	    mDraggableDot_Inbox.setOnDropListener(new MyHandlerInterface() {
			@Override
			public void onHandle(DraggableRect newcomer) {
				Log.v("TommyActivity", newcomer.getmLegend() + " has been dropped on Inbox.");
				((ViewGroup)newcomer.getParent()).removeView(newcomer);
				mLinearLayout_Inbox_Inner.addView(newcomer);
			}
		});
	    mDraggableDot_Inbox.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showInbox();
			}
		});
	    
	    mDraggableDot_Tasks.setOnDropListener(new MyHandlerInterface() {
			@Override
			public void onHandle(DraggableRect newcomer) {
				((ViewGroup)newcomer.getParent()).removeView(newcomer);
				mLinearLayout_Tasks_Inner.addView(newcomer);
			}
		});
	    mDraggableDot_Tasks.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showTasks();
			}
		});
	    
	    /* Feed some dummy data, boyz */
	    do_addDummyData("Press and hold to drag me!");
	    do_addDummyData("Betty bought a bit of better butter");
	    do_addDummyData("Drag & drop is Android 4.0.3 feature");
	    
	    SacrificeChicken();
	}
	
	private void addOneDummyData() {
		String s = mEditText1.getText().toString();
		mEditText1.setText("");
		do_addDummyData(s);
	}
	
	private void do_addDummyData(String wtf) {
		DraggableRect d = new DraggableRect(mCtx);
		d.setmLegend(wtf);
		d.setmWidth((int)(310*mCtx.getResources().getDisplayMetrics().density));
		d.setmHeight((int)(48*mCtx.getResources().getDisplayMetrics().density));
		mLinearLayout_Inbox_Inner.addView(d);
		mLinearLayout_Inbox_Inner.postInvalidate();
	}
	
	private void showInbox() {
		mLinearLayout_Inbox.setVisibility(View.VISIBLE);
		mLinearLayout_Tasks.setVisibility(View.GONE);
	}
	
	private void showTasks() {
		mLinearLayout_Inbox.setVisibility(View.GONE);
		mLinearLayout_Tasks.setVisibility(View.VISIBLE);
	}
	
	/* PATH-menu stuff */
	private void SacrificeChicken() {
		MyAnimations.which_corner = ANIM_CORNER.TOP_RIGHT;
		MyAnimations.initOffset(mCtx);
		composerButtonsWrapper = (RelativeLayout)findViewById(R.id.composer_buttons_wrapper);
		composerButtonsShowHideButton = (RelativeLayout)findViewById(R.id.composer_buttons_show_hide_button);
		composerButtonsShowHideButtonIcon = (ImageView) findViewById(R.id.composer_buttons_show_hide_button_icon);
		
		composerButtonsShowHideButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!isButtonsShown) {
					MyAnimations.startAnimationsIn(composerButtonsWrapper, 300);
					composerButtonsShowHideButtonIcon
							.startAnimation(MyAnimations.getRotateAnimation(0,
									-270, 300));
				} else {
					MyAnimations
							.startAnimationsOut(composerButtonsWrapper, 300);
					composerButtonsShowHideButtonIcon
							.startAnimation(MyAnimations.getRotateAnimation(
									-270, 0, 300));
				}
				isButtonsShown = !isButtonsShown;
			}
		});
		for (int i = 0; i < composerButtonsWrapper.getChildCount(); i++) {
			composerButtonsWrapper.getChildAt(i).setOnClickListener(
					new View.OnClickListener() {
						@Override
						public void onClick(View arg0) {
						}
					});
		}
		
		composerButtonsShowHideButton.startAnimation(MyAnimations
				.getRotateAnimation(0, 360, 200));

	}
}
