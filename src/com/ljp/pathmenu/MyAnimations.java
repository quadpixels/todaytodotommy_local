package com.ljp.pathmenu;

import android.content.Context;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageButton;

public class MyAnimations{
	
	/* 将“Offset”改成了“半长”/“半宽” */
	private static int	half_width		= 15;
	private static int	half_height		= 13;
	
	public enum ANIM_CORNER {
		TOP_LEFT,
		TOP_RIGHT,
		BOTTOM_LEFT,
		BOTTOM_RIGHT
	}
	public static ANIM_CORNER which_corner = ANIM_CORNER.TOP_LEFT;

	public static void initOffset(Context context){
		half_width		= (int) (10.667 *context.getResources().getDisplayMetrics().density);
		half_height		= (int) (8.667 *context.getResources().getDisplayMetrics().density);
	}
	
	public static Animation getRotateAnimation(float fromDegrees ,float toDegrees,int durationMillis){
		RotateAnimation rotate = new RotateAnimation(fromDegrees, toDegrees,
				Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
		rotate.setDuration(durationMillis);
		rotate.setFillAfter(true);
		return rotate;
	}

	public static void startAnimationsIn(ViewGroup viewgroup,int durationMillis) {
		for (int i = 0; i < viewgroup.getChildCount(); i++) {
				ImageButton inoutimagebutton = (ImageButton) viewgroup
						.getChildAt(i);
				inoutimagebutton.setVisibility(0);
				MarginLayoutParams mlp = (MarginLayoutParams) inoutimagebutton.getLayoutParams();
				Animation animation = null;
				/* Tommy, 07-21-2012
				 * Delta值是针对现在来说的。含义为：将这个东西从“自己向右deltaXStart个身位/pixel开始移动，
				 * 一直移动到向右deltaXEnd个Pixel。所以有时候会出现0。
				 *
				 */
				switch(which_corner) {
				case TOP_LEFT :
					/* 
					 * Tommy 2012-07-21：
					 * TOP_LEFT情况下，在Layout文件中直接指定属性里有表示这Layout的位置（是左/上Margin），所有delta都需加上
					 * 这几个位置才能得到屏幕上的位置。
					 */
					animation = new TranslateAnimation(-mlp.leftMargin + half_width, 0, 
															-mlp.topMargin + half_height,0);
					break;
				case TOP_RIGHT:
					/*
					 * TOP_RIGHT情况下指定的是右/上Margin
					 */
					animation = new TranslateAnimation(mlp.rightMargin - half_width, 0,
															-mlp.topMargin + half_height, 0);
					break;
				case BOTTOM_LEFT:
					/* TOP_LEFT: 左/下Margin */
					animation = new TranslateAnimation(-mlp.leftMargin + half_width, 0,
															mlp.bottomMargin - half_height, 0);
					break;
				case BOTTOM_RIGHT:
					/* TOP_RIGHT: 右/下Margin */
					animation = new TranslateAnimation(mlp.rightMargin + half_width, 0,
															mlp.bottomMargin - half_height, 0);
					break;
				}
				
				animation.setFillAfter(true);animation.setDuration(durationMillis);
				animation.setStartOffset((i * 100)
						/ (-1 + viewgroup.getChildCount()));
				animation.setInterpolator(new OvershootInterpolator(2F));
				inoutimagebutton.startAnimation(animation);
			
		}
	}
	public static void startAnimationsOut(ViewGroup viewgroup,int durationMillis) {
		for (int i = 0; i < viewgroup.getChildCount(); i++) {
				final ImageButton inoutimagebutton = (ImageButton) viewgroup
						.getChildAt(i);
				MarginLayoutParams mlp = (MarginLayoutParams) inoutimagebutton.getLayoutParams();
				Animation animation = null;
				switch(which_corner) {
				case TOP_LEFT:
					animation = new TranslateAnimation(0, -mlp.leftMargin + half_width, 
															0, -mlp.topMargin + half_height);
					break;
				case TOP_RIGHT:
					/*
					 * TOP_RIGHT情况下指定的是右/上Margin
					 */
					animation = new TranslateAnimation(0, mlp.rightMargin - half_width,
															0, -mlp.topMargin + half_height);
					break;
				case BOTTOM_LEFT:
					/* TOP_LEFT: 左/下Margin */
					animation = new TranslateAnimation(0, -mlp.leftMargin + half_width,
															0, mlp.bottomMargin - half_height);
					break;
				case BOTTOM_RIGHT:
					/* TOP_RIGHT: 右/下Margin */
					animation = new TranslateAnimation(0, mlp.rightMargin - half_width,
															0, mlp.bottomMargin - half_height);
					break;
				}
				
				animation.setFillAfter(true);animation.setDuration(durationMillis);
				animation.setStartOffset(((viewgroup.getChildCount()-i) * 100)
						/ (-1 + viewgroup.getChildCount()));
				animation.setInterpolator(new AnticipateInterpolator(2F));
				animation.setAnimationListener(new Animation.AnimationListener() {
					@Override
					public void onAnimationStart(Animation arg0) {}
					@Override
					public void onAnimationRepeat(Animation arg0) {}
					@Override
					public void onAnimationEnd(Animation arg0) {
						// TODO Auto-generated method stub
						inoutimagebutton.setVisibility(8);
					}
				});
				inoutimagebutton.startAnimation(animation);
			}
		
	}

	
}